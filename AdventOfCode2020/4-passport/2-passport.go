package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	// open text file
	f, err := os.Open("input.txt")
	//checks for error
	if err != nil {
		log.Fatal(err)
	}
	//tells it to close the file once program dies
	defer f.Close()
	// Defines list to be appended to
	listOfPassports := []string{}
	// Imports file scanner
	scanner := bufio.NewScanner(f)
	//Goes line by line and scans
	for scanner.Scan() {
		temp := scanner.Text()
		if err != nil {
			log.Fatal(err)
		}
		listOfPassports = append(listOfPassports, temp)
	}
	// checks for errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	// Declare 2d array for each line
	organizedListOfPassports := []string{}
	tempString := ""
	// Declares valid passports
	validPassports := 0
	_ = validPassports
	// This parses the list to make each line all the arguemnts for each passport
	for i, var1 := range listOfPassports {
		if var1 == "" || i == len(listOfPassports) {
			organizedListOfPassports = append(organizedListOfPassports, tempString)
			tempString = ""
		} else {
			tempString += var1 + " "
		}
		_, _ = i, var1
	}
	// Declare the passport arguments
	byr := ""
	iyr := ""
	eyr := ""
	hgt := ""
	hcl := ""
	ecl := ""
	pid := ""
	byrBool := false
	iyrBool := false
	eyrBool := false
	hgtBool := false
	hclBool := false
	eclBool := false
	pidBool := false
	var3 := ""
	// Numbers
	numbers := "1234567890"
	// Hair color things
	hairColorThings := "0123456789abcdef"
	// Now iterate through and check for valid passports
	for c, var2 := range organizedListOfPassports {
		println(var2)
		if strings.Contains(var2, "byr:") {
			var3 = var2[strings.Index(var2, "byr")+4:]
			byr = var3[:strings.Index(var3, " ")]
			byrInt, _ := strconv.Atoi(byr)
			if 1920 <= byrInt && 2002 >= byrInt {
				byrBool = true
			}
		}
		if strings.Contains(var2, "iyr:") {
			var3 = var2[strings.Index(var2, "iyr")+4:]
			iyr = var3[:strings.Index(var3, " ")]
			iyrInt, _ := strconv.Atoi(iyr)
			if 2010 <= iyrInt && 2020 >= iyrInt {
				iyrBool = true
			}
		}
		if strings.Contains(var2, "eyr:") {
			var3 = var2[strings.Index(var2, "eyr")+4:]
			eyr = var3[:strings.Index(var3, " ")]
			eyrInt, _ := strconv.Atoi(eyr)
			if 2020 <= eyrInt && 2030 >= eyrInt {
				eyrBool = true
			}
		}
		if strings.Contains(var2, "hgt:") {
			var3 = var2[strings.Index(var2, "hgt")+4:]
			hgt = var3[:strings.Index(var3, " ")]
			if hgt[len(hgt)-2:] == "cm" {
				hgtInt, _ := strconv.Atoi(hgt[:len(hgt)-2])
				if hgtInt >= 150 && hgtInt <= 193 {
					hgtBool = true
				}
			} else if hgt[len(hgt)-2:] == "in" {
				hgtInt, _ := strconv.Atoi(hgt[:len(hgt)-2])
				if hgtInt >= 59 && hgtInt <= 76 {
					hgtBool = true
				}
			}
		}
		if strings.Contains(var2, "hcl:") {
			var3 = var2[strings.Index(var2, "hcl")+4:]
			hcl = var3[:strings.Index(var3, " ")]
			if hcl[0:1] == "#" {
				counter1 := 1
				for d := 1; d < len(hcl); d++ {
					if strings.Contains(hairColorThings, string(hcl[d])) {
						counter1++
					}
				}
				if counter1 == len(hcl) {
					hclBool = true
				}
			}
		}
		if strings.Contains(var2, "ecl:") {
			var3 = var2[strings.Index(var2, "ecl")+4:]
			ecl = var3[:strings.Index(var3, " ")]
			if ecl == "amb" || ecl == "blu" || ecl == "brn" || ecl == "gry" || ecl == "grn" || ecl == "hzl" || ecl == "oth" {
				eclBool = true
			}
		}
		if strings.Contains(var2, "pid:") {
			var3 = var2[strings.Index(var2, "pid")+4:]
			pid = var3[:strings.Index(var3, " ")]
			if len(pid) == 9 {
				counter2 := 0
				for e := 0; e < len(pid); e++ {
					if strings.Contains(numbers, string(pid[e])) {
						counter2++
					}
				}
				if counter2 == 9 {
					pidBool = true
				}
			}
		}
		if byrBool == true && iyrBool == true && eyrBool == true && hgtBool == true && hclBool == true && eclBool == true && pidBool == true {
			println(var2)
			println("passed")
			validPassports++
			byr = ""
			iyr = ""
			eyr = ""
			hgt = ""
			hcl = ""
			ecl = ""
			pid = ""
			byrBool = false
			iyrBool = false
			eyrBool = false
			hgtBool = false
			hclBool = false
			eclBool = false
			pidBool = false
			var3 = ""
		} else {
			println(var2)
			println("failed")
			byr = ""
			iyr = ""
			eyr = ""
			hgt = ""
			hcl = ""
			ecl = ""
			pid = ""
			byrBool = false
			iyrBool = false
			eyrBool = false
			hgtBool = false
			hclBool = false
			eclBool = false
			pidBool = false
			var3 = ""
		}
		_, _, _, _, _, _, _, _, _ = c, var2, byr, iyr, eyr, hgt, hcl, ecl, pid
	}
	println(validPassports + 1)
}
