package main

import (
	"bufio"
	"log"
	"os"
	"strings"
)

func main() {
	// open text file
	f, err := os.Open("input.txt")
	//checks for error
	if err != nil {
		log.Fatal(err)
	}
	//tells it to close the file once program dies
	defer f.Close()
	// Defines list to be appended to
	listOfPassports := []string{}
	// Imports file scanner
	scanner := bufio.NewScanner(f)
	//Goes line by line and scans
	for scanner.Scan() {
		temp := scanner.Text()
		if err != nil {
			log.Fatal(err)
		}
		listOfPassports = append(listOfPassports, temp)
	}
	// checks for errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	// Declares valid passports
	validPassports := 0
	// Declare some bools
	byr := false
	iyr := false
	eyr := false
	hgt := false
	hcl := false
	ecl := false
	pid := false
	// Now checks for the things
	for c, var2 := range listOfPassports {
		println(var2)
		if strings.Contains(var2, "byr:") {
			byr = true
		}
		if strings.Contains(var2, "iyr:") {
			iyr = true
		}
		if strings.Contains(var2, "eyr:") {
			eyr = true
		}
		if strings.Contains(var2, "hgt:") {
			hgt = true
		}
		if strings.Contains(var2, "hcl:") {
			hcl = true
		}
		if strings.Contains(var2, "ecl:") {
			ecl = true
		}
		if strings.Contains(var2, "pid:") {
			pid = true
		}
		// Now if a break line happens, check for all of the different tags and if they exist, increment the valid passports thing
		if var2 == "" {
			if byr == true && iyr == true && eyr == true && hgt == true && hcl == true && ecl == true && pid == true {
				validPassports++
				byr = false
				iyr = false
				eyr = false
				hgt = false
				hcl = false
				ecl = false
				pid = false
				println("Got em all")
			} else {
				byr = false
				iyr = false
				eyr = false
				hgt = false
				hcl = false
				ecl = false
				pid = false
				println("doesn't contain all 7")
			}
		}
		_, _ = c, var2
	}
	println(validPassports + 1)
}
