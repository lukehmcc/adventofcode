package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
	// open text file
	f, err := os.Open("numbers.txt")
	//checks for error
	if err != nil {
		log.Fatal(err)
	}
	//tells it to close the file once program dies
	defer f.Close()
	// Defines list to be appended to
	listOfNumbers := []uint64{}
	// Imports file scanner
	scanner := bufio.NewScanner(f)
	// Defines counter
	counter := 1
	//Goes line by line and scans
	for scanner.Scan() {
		temp := scanner.Text()
		text2, err := strconv.ParseUint(temp, 10, 32)
		if err != nil {
			log.Fatal(err)
		}
		listOfNumbers = append(listOfNumbers, text2)
		counter++
	}
	// prints list
	fmt.Println(listOfNumbers)
	// checks for errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	//iterate through list and check for ones that sum to 2000
	for i, var1 := range listOfNumbers {
		for c, var2 := range listOfNumbers {
			for g, var3 := range listOfNumbers {
				if (var1 + var2 + var3) == 2020 {
					fmt.Println(var1 * var2 * var3)
					_ = i
					_ = c
					_ = g
				}
			}
		}
	}
}
