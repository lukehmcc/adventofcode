package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	commands := importText("input.txt")
	fmt.Println(commands)
	acc := parse(commands)
	println("total", acc)
	// backwards := backwards(commands)
	// println("total back", backwards)
}

// Part 1
func parse(commands []string) int {
	acc := 0
	visited := []bool{}
	for i := 0; i < len(commands); i++ {
		visited = append(visited, false)
	}
	for i := 0; i < len(commands); i++ {
		println("i = ", i, ": visted:", visited[i], ": command:", commands[i])
		if visited[i] == true {
			return acc
		}
		visited[i] = true
		if strings.Contains(commands[i], "nop") {
			println("no operation")
		} else if strings.Contains(commands[i], "acc") {
			if strings.Contains(commands[i], "+") {
				temp, _ := strconv.Atoi(commands[i][5:])
				println(commands[i], " adding ", temp)
				acc += temp
			} else {
				temp, _ := strconv.Atoi(commands[i][5:])
				acc -= temp
			}
		} else {
			if strings.Contains(commands[i], "+") {
				temp, _ := strconv.Atoi(commands[i][5:])
				println(commands[i], " i is being pushed foward ", temp)
				i += temp - 1
			} else {
				temp, _ := strconv.Atoi(commands[i][5:])
				println(commands[i], " i is being pushed back ", temp)
				if -1*temp < 0 {
					i += (-1 * temp) - 1
				}
			}
		}
	}
	return acc
}

// Part 2
func backwards(commands []string) int {
	hasPassed := false
	iterator := 0
	tempi := 0
	acc := 0
	for hasPassed == false {
		acc = 0
		visited := []bool{}
		for i := 0; i < len(commands); i++ {
			visited = append(visited, false)
		}
		for i := 0; i < len(commands); i++ {
			tempi = i
			println("i = ", i, ": visted:", visited[i], ": command:", commands[i])
			if visited[i] == true {
				break
			}
			visited[i] = true
			if i != iterator {
				if strings.Contains(commands[i], "nop") {
					println("no operation")
				} else if strings.Contains(commands[i], "acc") {
					if strings.Contains(commands[i], "+") {
						temp, _ := strconv.Atoi(commands[i][5:])
						println(commands[i], " adding ", temp)
						acc += temp
					} else {
						temp, _ := strconv.Atoi(commands[i][5:])
						acc -= temp
					}
				} else if strings.Contains(commands[i], "jmp") {
					if strings.Contains(commands[i], "+") {
						temp, _ := strconv.Atoi(commands[i][5:])
						println(commands[i], " i is being pushed foward ", temp)
						i += temp - 1
					} else {
						temp, _ := strconv.Atoi(commands[i][5:])
						println(commands[i], " i is being pushed back ", temp)
						if -1*temp < 0 {
							i += (-1 * temp) - 1
						}
					}
				}
			} else {
				if strings.Contains(commands[i], "jmp") {
					println("no operation")
				} else if strings.Contains(commands[i], "acc") {
					if strings.Contains(commands[i], "+") {
						temp, _ := strconv.Atoi(commands[i][5:])
						println(commands[i], " adding ", temp)
						acc += temp
					} else {
						temp, _ := strconv.Atoi(commands[i][5:])
						acc -= temp
					}
				} else if strings.Contains(commands[i], "nop") {
					if strings.Contains(commands[i], "+") {
						temp, _ := strconv.Atoi(commands[i][5:])
						println(commands[i], " i is being pushed foward ", temp)
						i += temp - 1
					} else {
						temp, _ := strconv.Atoi(commands[i][5:])
						println(commands[i], " i is being pushed back ", temp)
						if -1*temp < 0 {
							i += (-1 * temp) - 1
						}
					}
				}
			}
		}
		if tempi == len(commands)-1 {
			hasPassed = true
			print("The thing that needed to be changed was line", iterator)
		} else if iterator > len(commands) {
			return 0
		} else {
			iterator++
			println("------------------------------------------------", iterator, "-----------------------------------------------------------")
		}
	}
	return acc
}

// Import text
func importText(fileName string) []string {
	// open text file
	f, err := os.Open(fileName)
	//checks for error
	if err != nil {
		log.Fatal(err)
	}
	//tells it to close the file once program dies
	defer f.Close()
	// Defines list to be appended to
	listOfThings := []string{}
	// Imports file scanner
	scanner := bufio.NewScanner(f)
	//Goes line by line and scans
	for scanner.Scan() {
		temp := scanner.Text()
		listOfThings = append(listOfThings, temp)
	}
	// checks for errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return listOfThings
}
