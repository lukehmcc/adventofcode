package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	// open text file
	f, err := os.Open("input.txt")
	//checks for error
	if err != nil {
		log.Fatal(err)
	}
	//tells it to close the file once program dies
	defer f.Close()
	// Defines list to be appended to
	listOfPasswords := []string{}
	// Imports file scanner
	scanner := bufio.NewScanner(f)
	// Defines counter
	counter := 1
	//Goes line by line and scans
	for scanner.Scan() {
		listOfPasswords = append(listOfPasswords, scanner.Text())
		counter++
	}
	// prints list
	// checks for errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	goodPasswords := 0
	for i, var1 := range listOfPasswords {
		_ = i
		var2 := strings.Join(strings.Fields(var1), "")
		dashIndex := strings.Index(var2, "-")
		colonIndex := strings.Index(var2, ":")
		index1 := string(var2[0:dashIndex])
		index2 := string(var2[(dashIndex + 1) : colonIndex-1])
		letter := string(var2[colonIndex-1 : colonIndex])
		password := string(var2[colonIndex+1 : len(var2)])
		//convert indexes to ints
		lowInt, err := strconv.Atoi(index1)
		if err != nil {
			log.Fatal(err)
		}
		highInt, err := strconv.Atoi(index2)
		if err != nil {
			log.Fatal(err)
		}
		if lowInt == 0 {
			highInt--
		} else {
			lowInt--
			highInt--
		}
		if len(password) > highInt {
			if string(password[lowInt]) != letter && string(password[highInt]) == letter {
				goodPasswords++
			} else if string(password[lowInt]) == letter && string(password[highInt]) != letter {
				goodPasswords++
			}
		}
		_, _, _ = letter, highInt, lowInt
	}
	fmt.Println(goodPasswords)
}
