package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
	// Find the manhattan PART ONE
	directions := importText("input.txt")
	manhat := findManhat(directions)
	println("Total manhattan distance:", manhat)
	// Fine the actual manhattan PART TWO
	manhatReal := findManhatReal(directions)
	println("Total real manhattan distance:", manhatReal)
}

// PART ONE find manhattan
func findManhat(directions []string) int {
	// define accumulators and the likes
	nAcc := 0
	eAcc := 0
	sAcc := 0
	wAcc := 0
	temp := 0
	rotPos := 1
	// Go through the list and do logic
	for i := 0; i < len(directions); i++ {
		if string(directions[i][:1]) == "N" {
			temp, _ = strconv.Atoi(directions[i][1:])
			nAcc += temp
		} else if string(directions[i][:1]) == "E" {
			temp, _ = strconv.Atoi(directions[i][1:])
			eAcc += temp
		} else if string(directions[i][:1]) == "S" {
			temp, _ = strconv.Atoi(directions[i][1:])
			sAcc += temp
		} else if string(directions[i][:1]) == "W" {
			temp, _ = strconv.Atoi(directions[i][1:])
			wAcc += temp
		} else if string(directions[i][:1]) == "R" {
			temp, _ = strconv.Atoi(directions[i][1:])
			rotPos += temp / 90
			for rotPos > 3 {
				rotPos -= 4
			}
		} else if string(directions[i][:1]) == "L" {
			temp, _ = strconv.Atoi(directions[i][1:])
			rotPos -= temp / 90
			for rotPos < 0 {
				rotPos += 4
			}
		} else if string(directions[i][:1]) == "F" {
			temp, _ = strconv.Atoi(directions[i][1:])
			if rotPos == 0 {
				nAcc += temp
			} else if rotPos == 1 {
				eAcc += temp
			} else if rotPos == 2 {
				sAcc += temp
			} else if rotPos == 3 {
				wAcc += temp
			}
		}
	}
	// println("north acc", nAcc)
	// println("east acc", eAcc)
	// println("south acc", sAcc)
	// println("west acc", wAcc)
	return abs(nAcc-sAcc) + abs(eAcc-wAcc)
}

// PART ONE find manhattan
func findManhatReal(directions []string) int {
	// define accumulators and the likes
	currPoint := []int{0, 0}
	temp := 0
	waypoint := []int{10, 1}
	// Go through the list and do logic
	for i := 0; i < len(directions); i++ {
		// print stuff
		println("------------")
		fmt.Println("command curr", directions[i])
		fmt.Println("waypoint currbef", waypoint)
		fmt.Println("pos currbef", currPoint)
		if string(directions[i][:1]) == "N" {
			temp, _ = strconv.Atoi(directions[i][1:])
			waypoint[1] += temp
		} else if string(directions[i][:1]) == "E" {
			temp, _ = strconv.Atoi(directions[i][1:])
			waypoint[0] += temp
		} else if string(directions[i][:1]) == "S" {
			temp, _ = strconv.Atoi(directions[i][1:])
			waypoint[1] -= temp
		} else if string(directions[i][:1]) == "W" {
			temp, _ = strconv.Atoi(directions[i][1:])
			waypoint[0] -= temp
		} else if string(directions[i][:1]) == "R" {
			temp, _ = strconv.Atoi(directions[i][1:])
			for i := 0; i < temp/90; i++ {
				x := waypoint[1]
				y := waypoint[0] * -1
				waypoint = []int{}
				waypoint = append(waypoint, x)
				waypoint = append(waypoint, y)
			}
		} else if string(directions[i][:1]) == "L" {
			temp, _ = strconv.Atoi(directions[i][1:])
			for i := 0; i < temp/90; i++ {
				x := waypoint[1] * -1
				y := waypoint[0]
				waypoint = []int{}
				waypoint = append(waypoint, x)
				waypoint = append(waypoint, y)
			}
		} else if string(directions[i][:1]) == "F" {
			temp, _ = strconv.Atoi(directions[i][1:])
			currPoint[0] += waypoint[0] * temp
			currPoint[1] += waypoint[1] * temp
		}

		fmt.Println("waypoint curr", waypoint)
		fmt.Println("pos curr", currPoint)
	}

	return abs(currPoint[0]) + abs(currPoint[1])
}

// Import text
func importText(fileName string) []string {
	// open text file
	f, err := os.Open(fileName)
	//checks for error
	if err != nil {
		log.Fatal(err)
	}
	//tells it to close the file once program dies
	defer f.Close()
	// Defines list to be appended to
	listOfThings := []string{}
	// Imports file scanner
	scanner := bufio.NewScanner(f)
	//Goes line by line and scans
	for scanner.Scan() {
		temp := scanner.Text()
		listOfThings = append(listOfThings, temp)
	}
	// checks for errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return listOfThings
}

// finds absolute value
func abs(num int) int {
	if num < 0 {
		num *= -1
	}
	return num
}
