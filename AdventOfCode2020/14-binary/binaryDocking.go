package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

func main() {
	// Part one, sums the memory
	listOfThings := importText("input.txt")
	parsedList := parseData(listOfThings)
	// fmt.Println(parsedList)
	valSum := sumMemoryValueMask(parsedList)
	fmt.Println("address sum value mask:", valSum)
	addrSum := sumMemoryAdressMask(parsedList)
	fmt.Println("address sum address mask", addrSum)

}

// Find PART ONE sumMemory
func sumMemoryValueMask(parsedList [][]string) int {
	// First parse it
	// Turns to string:
	// var1 := fmt.Sprintf("%036s", strconv.FormatUint(4739, 2))
	// Turns to binary
	// var2, _ := strconv.ParseUint(var1, 2, 64)

	// Defines some stuff
	tempMask := ""
	toAppend := ""
	// Okay so I'm gonna be using a map for mem addreses
	memoryMap := make(map[int]uint64)
	for i := 0; i < len(parsedList); i++ {
		for c := 0; c < len(parsedList[i]); c++ {
			if c == 0 {
				tempMask = parsedList[i][c]
				// println(tempMask)
			} else {
				// Turns binary to littleEndy 36 long binary
				index := strings.Index(parsedList[i][c], "=")
				tempInt, _ := strconv.Atoi(parsedList[i][c][index+2:])
				uIntTempInt := uint64(tempInt)
				fullBin := fmt.Sprintf("%036s", strconv.FormatUint(uIntTempInt, 2))
				// println("-------")
				// println(" premask:", fullBin)
				// println("    mask:", tempMask)
				// Now masks
				toAppend = ""
				for j := 0; j < len(tempMask); j++ {
					if tempMask[j:j+1] == "X" {
						toAppend += fullBin[j : j+1]
					} else if tempMask[j:j+1] == "0" {
						toAppend += "0"
					} else if tempMask[j:j+1] == "1" {
						toAppend += "1"
					}
				}
				// println("postmask:", toAppend)
				// Now turn that back to an int and drop that it in the memory addr
				toMemory, _ := strconv.ParseUint(toAppend, 2, 64)
				// println("That is now:", toMemory)
				startIndex := strings.Index(parsedList[i][c], "[")
				endIndex := strings.Index(parsedList[i][c], "]")
				memLoc, _ := strconv.Atoi(parsedList[i][c][startIndex+1 : endIndex])
				memoryMap[memLoc] = toMemory
			}
		}
	}
	sum := 0
	for val := range memoryMap {
		sum += int(memoryMap[val])
	}
	return sum
}

// Part 2 masks the address instead of the value
func sumMemoryAdressMask(parsedList [][]string) int {
	// fmt.Println(parsedList)
	tempMask := ""
	toAppend := ""
	// Okay so I'm gonna be using a map for mem addreses
	memoryMap := make(map[int]int)
	for i := 0; i < len(parsedList); i++ {
		for c := 0; c < len(parsedList[i]); c++ {
			if c == 0 {
				tempMask = parsedList[i][c]
				// println(tempMask)
			} else {
				// find the mem address
				startIndex := strings.Index(parsedList[i][c], "[")
				endIndex := strings.Index(parsedList[i][c], "]")
				memLoc, _ := strconv.Atoi(parsedList[i][c][startIndex+1 : endIndex])
				// Now convert that to bin
				uIntTempInt := uint64(memLoc)
				fullBinAddr := fmt.Sprintf("%036s", strconv.FormatUint(uIntTempInt, 2))
				// Print it out for analysis purposes
				// println("-------------")
				// println("memAddr:", fullBinAddr)
				// println("   Mask:", tempMask)
				// Now mask without dealing with the x's
				toAppend = ""
				for j := 0; j < len(tempMask); j++ {
					if tempMask[j:j+1] == "X" {
						toAppend += "X"
					} else if tempMask[j:j+1] == "0" {
						toAppend += fullBinAddr[j : j+1]
					} else if tempMask[j:j+1] == "1" {
						toAppend += "1"
					}
				}
				// println("   preX:", toAppend)
				//Now break those xes out and append them to a list
				listOfAddresses := []string{}
				manyXes := findX(toAppend)
				possibleCombos := powInt(2, manyXes)
				for j := 0; j < possibleCombos; j++ {
					sprintString := "%0" + fmt.Sprint(manyXes) + "s"
					nums := fmt.Sprintf(sprintString, strconv.FormatUint(uint64(j), 2))
					temp := toAppend
					for k := 0; k < len(nums); k++ {
						index := strings.Index(temp, "X")
						temp = temp[:index] + nums[k:k+1] + temp[index+1:]
					}
					// println("binary Temp", temp)
					listOfAddresses = append(listOfAddresses, temp)
				}
				// fmt.Println(listOfAddresses)
				// Now converto those mem address into ints and assign the values
				for j := 0; j < len(listOfAddresses); j++ {
					memAddr, _ := strconv.ParseUint(listOfAddresses[j], 2, 64)
					memAddrInt := int(memAddr)
					index := strings.Index(parsedList[i][c], "=")
					tempVal, _ := strconv.Atoi(parsedList[i][c][index+2:])
					// println("index:", memAddrInt, "| val:", tempVal)
					memoryMap[memAddrInt] = tempVal
				}
			}
		}
	}
	sum := 0
	for val := range memoryMap {
		// println(memoryMap[val])
		sum += int(memoryMap[val])
	}
	return sum
}

// Finds the amount of X in strings
func findX(input string) int {
	numX := 0
	for i := 0; i < len(input); i++ {
		if string(input[i]) == "X" {
			numX++
		}
	}
	return numX
}

// Raises to power
func powInt(x, y int) int {
	return int(math.Pow(float64(x), float64(y)))
}

// Parses input
func parseData(listOfThings []string) [][]string {
	parsedData := [][]string{}
	temp2 := []string{}
	for i := 0; i < len(listOfThings); i++ {
		if listOfThings[i][:4] == "mask" {
			if i != 0 {
				parsedData = append(parsedData, temp2)
			}
			temp2 = []string{}
			temp2 = append(temp2, listOfThings[i][7:])
		} else if i == len(listOfThings)-1 {
			temp2 = append(temp2, listOfThings[i])
			parsedData = append(parsedData, temp2)
		} else {
			temp2 = append(temp2, listOfThings[i])
		}
	}
	return parsedData
}

// Import text
func importText(fileName string) []string {
	// open text file
	f, err := os.Open(fileName)
	//checks for error
	if err != nil {
		log.Fatal(err)
	}
	//tells it to close the file once program dies
	defer f.Close()
	// Defines list to be appended to
	listOfThings := []string{}
	// Imports file scanner
	scanner := bufio.NewScanner(f)
	//Goes line by line and scans
	for scanner.Scan() {
		temp := scanner.Text()
		listOfThings = append(listOfThings, temp)
	}
	// checks for errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return listOfThings
}
