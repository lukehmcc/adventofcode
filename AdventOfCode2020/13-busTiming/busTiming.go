package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	// PART ONE
	inputText := importText("input.txt")
	timeToLeave := findTiming(inputText)
	println("The time to leave is:", timeToLeave)
}

// PART ONE finds closest bus
func findTiming(inputText []string) int {
	// Define some vars and parse text
	timeToBeNear, _ := strconv.Atoi(inputText[0])
	splitInput := strings.Split(inputText[1], ",")
	splint := []int{}
	lowest := []int{10000000000, 1000000000}
	temp := 0
	// Get rid of the x's and convert to ints
	for i := 0; i < len(splitInput); i++ {
		if string(splitInput[i]) != "x" {
			temp, _ = strconv.Atoi(splitInput[i])
			splint = append(splint, temp)
		}
	}
	// Now find that timing
	for i := 0; i < len(splint); i++ {
		temp = 0
		for j := 0; temp < timeToBeNear; j++ {
			temp += splint[i]
		}
		if temp-timeToBeNear < lowest[1]-timeToBeNear {
			lowest = []int{splint[i], temp}
		}
	}
	// Do some prints
	println("ID:", lowest[0])
	println("diff:", lowest[1]-timeToBeNear)
	// Return the ID * the differnce between the wanted time and
	// the nearest bus
	return lowest[0] * (lowest[1] - timeToBeNear)
}

// Import text
func importText(fileName string) []string {
	// open text file
	f, err := os.Open(fileName)
	//checks for error
	if err != nil {
		log.Fatal(err)
	}
	//tells it to close the file once program dies
	defer f.Close()
	// Defines list to be appended to
	listOfThings := []string{}
	// Imports file scanner
	scanner := bufio.NewScanner(f)
	//Goes line by line and scans
	for scanner.Scan() {
		temp := scanner.Text()
		listOfThings = append(listOfThings, temp)
	}
	// checks for errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return listOfThings
}
