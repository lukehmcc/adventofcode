package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
	// Imports text
	listOfStrings := importText("input.txt")
	// Turns that text to ints
	listOfNums := stringToNum(listOfStrings)
	// fmt.Println(listOfNums)
	// Sauces that over the the logic function
	finNum := incorrectNum(listOfNums)
	println("the incorrect number was:", finNum)
	breakKey := contiguousSum(finNum, listOfNums)
	println("the break key is:", breakKey)
}

// Finds the incorrectnum
func incorrectNum(listOfNums []int) int {
	// Declares var to use alter
	doesAdd := false
	// Iterates through all the numbers besides preamble
	for i := 25; i < len(listOfNums); i++ {
		doesAdd = false
		//Iterates through the previous 5 numbers
		for l := i - 25; l < i; l++ {
			// Iterates through the last 5 numbers
			for m := i - 25; m < i; m++ {
				// Checks if the numbers added = the current number
				if listOfNums[l]+listOfNums[m] == listOfNums[i] {
					doesAdd = true
				}
			}
		}
		// If the number doesn't add up(ha) it passes it back
		if doesAdd != true {
			return listOfNums[i]
		}
	}
	return 1
}

// Turns a list of strings to a list of nums
func stringToNum(listOfStrings []string) []int {
	listOfInts := []int{}
	for i := 0; i < len(listOfStrings); i++ {
		temp, _ := strconv.Atoi(listOfStrings[i])
		listOfInts = append(listOfInts, temp)
	}
	return listOfInts
}

// contiguous sum
func contiguousSum(incorrectNum int, listOfNums []int) int {
	// Find index of the incorrect sumber
	incorrectIndex := 0
	for i := 0; i < len(listOfNums); i++ {
		if listOfNums[i] == incorrectNum {
			incorrectIndex = i
		}
	}
	// A temp var to store current contigious sum
	tempSum := 0
	tempArray := []int{}
	//Now go through and find those contigious sumbers
	for i := 0; i < incorrectIndex; i++ {
		tempSum = 0
		tempArray = []int{}
		// Goes up the list and keeps adding numbers
		for l := i; l < incorrectIndex; l++ {
			tempSum += listOfNums[l]
			if tempSum == incorrectNum {
				tempArray = append(tempArray, listOfNums[l])
				fmt.Println(tempArray)
				min, max := minimumMax(tempArray)
				println("start:", i, "end:", l, "min", min, "max", max)
				return min + max
			} else {
				tempArray = append(tempArray, listOfNums[l])
			}
		}
	}
	return 1
}

//Minimum
func minimumMax(numArray []int) (int, int) {
	min := 1000000000000000000
	max := 0
	for i := 0; i < len(numArray); i++ {
		if numArray[i] > max {
			max = numArray[i]
		}
	}
	for i := 0; i < len(numArray); i++ {
		if numArray[i] < min {
			min = numArray[i]
		}
	}
	return min, max
}

// Import text
func importText(fileName string) []string {
	// open text file
	f, err := os.Open(fileName)
	//checks for error
	if err != nil {
		log.Fatal(err)
	}
	//tells it to close the file once program dies
	defer f.Close()
	// Defines list to be appended to
	listOfThings := []string{}
	// Imports file scanner
	scanner := bufio.NewScanner(f)
	//Goes line by line and scans
	for scanner.Scan() {
		temp := scanner.Text()
		listOfThings = append(listOfThings, temp)
	}
	// checks for errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return listOfThings
}
