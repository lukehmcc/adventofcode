package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
	"unicode"
)

// Main function
func main() {
	listOfRules := importText("input.txt")
	mapArray := parseBags(listOfRules)
	numGold := findGold(mapArray)
	println("number of bags that reduce to gold", numGold)
}

// Import text
func importText(fileName string) []string {
	// open text file
	f, err := os.Open(fileName)
	//checks for error
	if err != nil {
		log.Fatal(err)
	}
	//tells it to close the file once program dies
	defer f.Close()
	// Defines list to be appended to
	listOfThings := []string{}
	// Imports file scanner
	scanner := bufio.NewScanner(f)
	//Goes line by line and scans
	for scanner.Scan() {
		temp := scanner.Text()
		listOfThings = append(listOfThings, temp)
	}
	// checks for errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return listOfThings
}

// Finds golden bags
func findGold(mapArray map[string][]string) int {
	//Make a copy of the map
	mapArrayOrig := make(map[string][]string)
	for k, v := range mapArray {
		mapArrayOrig[k] = v
	}
	numGold := 0
	tempArray := []string{}
	printMap(mapArray)
	for len(mapArray) > 0 {
		for key := range mapArray {
			if stringInSlice("shiny gold bags", mapArray[key]) || stringInSlice("shiny gold bag", mapArray[key]) {
				numGold++
				delete(mapArray, key)
			} else if len(mapArray[key]) != 0 && mapArray[key][0] == "no other bags" && len(mapArray[key]) == 1 {
				delete(mapArray, key)
			} else if len(mapArray[key]) == 0 {
				delete(mapArray, key)
			} else {
				for l := 0; l < len(mapArray[key]); l++ {
					if len(mapArray[mapArray[key][l]]) != 0 {
						for m, temp3 := range mapArrayOrig[mapArray[key][l]] {
							tempArray = mapArrayOrig[mapArray[key][l]]
							mapArray[key] = append(mapArray[key], tempArray[m])
							_ = temp3
						}
					}
					mapArray[key] = remove(mapArray[key], l)
					mapArray[key] = removeDupes(mapArray[key])
				}
			}
			_ = key
		}
		printMap(mapArray)
		printMap(mapArrayOrig)
	}

	return numGold
}

// parses the bags array
func parseBags(rule1D []string) map[string][]string {
	// Defines some variables to be used later
	nextNew := true
	tempArray := []string{}
	mapArray := make(map[string][]string)
	// Does the brunt of the parsing
	for i := 0; i < len(rule1D); i++ {
		temp := strings.Split(rule1D[i], " contain ")
		for c := 0; c < len(temp); c++ {
			mapArray = appendList(temp, nextNew, tempArray, mapArray, c)
		}
	}
	return mapArray
}
func appendList(temp []string, nextNew bool, tempArray []string, mapArray map[string][]string, c int) map[string][]string {
	if c == 0 {
		// println(temp[c])
	} else {
		if strings.Contains(temp[c], ",") {
			temp2 := strings.Split(temp[c], ", ")
			for j := 0; j < len(temp2); j++ {
				if unicode.IsDigit(rune(temp2[j][0])) {
					temp2[j] = temp2[j][2:]
				}
				if strings.Contains(temp2[j], ".") {
					tempArray = append(tempArray, temp2[j][:len(temp2[j])-1])
					mapArray[temp[0]] = tempArray
					tempArray = []string{}
				} else {
					tempArray = append(tempArray, temp2[j])
				}
			}
		} else {
			if unicode.IsDigit(rune(temp[1][0])) {
				temp[1] = temp[1][2:]
			}
			if strings.Contains(temp[1], ".") {
				tempArray = append(tempArray, temp[1][:len(temp[1])-1])
				mapArray[temp[0]] = tempArray
				tempArray = []string{}
			} else {
				tempArray = append(tempArray, temp[1])
			}
		}
	}
	return mapArray
}

// Checks how many times to dupe
func numMult(bagInfo string) int {
	if string(bagInfo[0]) == "1" {
		return 1
	} else if string(bagInfo[0]) == "2" {
		return 1
	} else if string(bagInfo[0]) == "3" {
		return 1
	} else if string(bagInfo[0]) == "4" {
		return 1
	} else if string(bagInfo[0]) == "5" {
		return 1
	} else if string(bagInfo[0]) == "6" {
		return 1
	} else if string(bagInfo[0]) == "7" {
		return 1
	} else if string(bagInfo[0]) == "8" {
		return 1
	} else if string(bagInfo[0]) == "9" {
		return 1
	} else {
		return 0
	}
}

// checks if string is in array
func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// Removes index of slice
func remove(s []string, i int) []string {
	s[len(s)-1], s[i] = s[i], s[len(s)-1]
	return s[:len(s)-1]
}

func removeDupes(a []string) []string {

	check := make(map[string]int)
	res := make([]string, 0)
	for _, val := range a {
		check[val] = 1
	}

	for letter := range check {
		res = append(res, letter)
	}

	return res
}

// prints maps
func printMap(mapArray map[string][]string) {
	println("----------------------------")
	for index, element := range mapArray {
		fmt.Println(index, "=>", element)
	}
}
