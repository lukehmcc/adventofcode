package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"math/bits"
	"os"
	"sort"
	"strconv"
)

func main() {
	// Part 1
	listOfInts := importTextInt("input.txt")
	fmt.Println(listOfInts)
	jump1, jump2, jump3 := findJumps(listOfInts)
	fmt.Println("1 jumps:", jump1, "2 jumps:", jump2, "3 jumps:", jump3)
	println("Multiplied:", jump1*jump3)
	// Part 2
	combos := totalCombos(listOfInts)
	println("Total combos:", combos)
}

// Finds jumps PART 1
func findJumps(listOfInts []int) (int, int, int) {
	//Sorts list
	sort.Ints(listOfInts)
	//defines vars
	jump1 := 0
	jump2 := 0
	jump3 := 0
	temp := 0
	// Iterates through list of vars
	for i := 0; i < len(listOfInts); i++ {
		// Checks for the first index so you don't get an "index out of bounds" error
		if i == 0 {
			temp = listOfInts[i]
		} else {
			temp = listOfInts[i] - listOfInts[i-1]
		}
		// Checks how many stops it jumps and iterates the thingies
		if temp == 1 {
			jump1++
		} else if temp == 2 {
			jump2++
		} else if temp == 3 {
			jump3++
		} else {
			return 0, 0, 0
		}
	}
	// Adds one to the "3 jump" catigory because the final adapter to the device is a 3 jolt jump
	// then returns
	return jump1, jump2, jump3 + 1
}

// Finds combos
func totalCombos(listOfInts []int) int {
	powerSet := Combinations(listOfInts, 0)
	fmt.Println(powerSet)
	return 2
}

// Check the list
func checkList(listOfInts []int) bool {
	sort.Ints(listOfInts)
	//defines vars
	jump1 := 0
	jump2 := 0
	jump3 := 0
	temp := 0
	// Iterates through list of vars
	for i := 0; i < len(listOfInts); i++ {
		// Checks for the first index so you don't get an "index out of bounds" error
		if i == 0 {
			temp = listOfInts[i]
		} else {
			temp = listOfInts[i] - listOfInts[i-1]
		}
		// Checks how many stops it jumps and iterates the thingies
		if temp == 1 {
			jump1++
		} else if temp == 2 {
			jump2++
		} else if temp == 3 {
			jump3++
		} else {
			return false
		}
	}
	// Adds one to the "3 jump" catigory because the final adapter to the device is a 3 jolt jump
	// then returns
	return true
}

// Finds all possible combos
func powerSet(original []int) [][]int {
	powerSetSize := int(math.Pow(2, float64(len(original))))
	println("power set size:", powerSetSize)
	result := [][]int{}
	var index int
	for index < powerSetSize {
		var subSet []int

		for j, elem := range original {
			if index&(1<<uint(j)) > 0 {
				subSet = append(subSet, elem)
			}
		}
		println("appending string")
		result = append(result, subSet)
		index++
	}
	return result
}

// Import text
func importTextInt(fileName string) []int {
	// open text file
	f, err := os.Open(fileName)
	//checks for error
	if err != nil {
		log.Fatal(err)
	}
	//tells it to close the file once program dies
	defer f.Close()
	// Defines list to be appended to
	listOfThings := []int{}
	// Imports file scanner
	scanner := bufio.NewScanner(f)
	//Goes line by line and scans
	for scanner.Scan() {
		temp, _ := strconv.Atoi(scanner.Text())
		listOfThings = append(listOfThings, temp)
	}
	// checks for errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return listOfThings
}

// I ran out of time so I stole this from github com/mxschmitt/golang-combinations/
// Combinations returns combinations of n elements for a given string array.
// For n < 1, it equals to All and returns all combinations.
func Combinations(set []int, n int) (subsets [][]int) {
	length := uint(len(set))

	if n > len(set) {
		n = len(set)
	}

	// Go through all possible combinations of objects
	// from 1 (only first object in subset) to 2^length (all objects in subset)
	for subsetBits := 1; subsetBits < (1 << length); subsetBits++ {
		if n > 0 && bits.OnesCount(uint(subsetBits)) != n {
			continue
		}

		var subset []int

		for object := uint(0); object < length; object++ {
			// checks if object is contained in subset
			// by checking if bit 'object' is set in subsetBits
			if (subsetBits>>object)&1 == 1 {
				// add object to subset
				subset = append(subset, set[object])
			}
		}
		// add subset to subsets
		subsets = append(subsets, subset)
	}
	return subsets
}
