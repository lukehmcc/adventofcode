package main

import (
	"bufio"
	"log"
	"os"
	"reflect"
)

func main() {
	// Import text
	array := importText("input.txt")
	// return how many steps it takes to get the seating to chill
	occupied := findSeating(array)
	println("This many seats are occupied:", occupied)
	// Turn the array to 2D and send it to part 2
	array = importText("input.txt")
	array2D := make2D(array)
	occupied2 := findSeating2(array2D)
	println("This many seats are occupied now:", occupied2)

}

// PART ONE
func findSeating(array []string) int {
	//Define some vars
	occupied := 0
	arrayOld := []string{}
	nearSeats := 0
	// Create an old aray
	for reflect.DeepEqual(array, arrayOld) == false {
		arrayOld = []string{}
		//Print out the array and iteration
		for i := 0; i < len(array); i++ {
			arrayOld = append(arrayOld, array[i])
		}
		//Iterate through each char
		for i := 0; i < len(arrayOld); i++ {
			for l := 0; l < len(arrayOld[i]); l++ {
				nearSeats = 0
				// now apply some rules
				// Check if it's floor
				if string(arrayOld[i][l]) == "." {
					nearSeats -= 120
				}
				if l-1 >= 0 && string(arrayOld[i][l-1]) == "#" { // Check left one
					nearSeats++
				}
				if i-1 >= 0 && string(arrayOld[i-1][l]) == "#" { // Check up one
					nearSeats++
				}
				if l+1 < len(arrayOld[i]) && string(arrayOld[i][l+1]) == "#" { // Check right one
					nearSeats++
				}
				if i+1 < len(arrayOld) && string(arrayOld[i+1][l]) == "#" { // Check down one
					nearSeats++
				}
				if l-1 >= 0 && i-1 >= 0 && string(arrayOld[i-1][l-1]) == "#" { // Check <^
					nearSeats++
				}
				if l+1 < len(arrayOld[i]) && i-1 >= 0 && string(arrayOld[i-1][l+1]) == "#" { // Check >^
					nearSeats++
				}
				if l-1 >= 0 && i+1 < len(arrayOld) && string(arrayOld[i+1][l-1]) == "#" { // Check <\/
					nearSeats++
				}
				if l+1 < len(arrayOld[i]) && i+1 < len(arrayOld) && string(arrayOld[i+1][l+1]) == "#" { // Check >\/
					nearSeats++
				}
				if nearSeats == 0 { // now either empty or fill
					array[i] = string(array[i][:l]) + "#" + string(array[i][l+1:])
				} else if nearSeats >= 4 {
					array[i] = string(array[i][:l]) + "L" + string(array[i][l+1:])
				}
			}
		}
	}
	// Now find the amount occupied
	for i := 0; i < len(array); i++ {
		for l := 0; l < len(array[i]); l++ {
			if string(array[i][l]) == "#" {
				occupied++
			}
		}
	}
	return occupied

}

// Part 2
func findSeating2(array2D [][]string) int {
	// Define some vars
	ocupado := 0
	arrayOld2D := [][]string{}
	nearSeats := 0
	// Now find those occupied seats!
	for reflect.DeepEqual(array2D, arrayOld2D) == false {
		arrayOld2D = [][]string{}
		//Print out the array and iteration
		// println("----------------------------")
		arrayTemp := []string{}
		for i := 0; i < len(array2D); i++ {
			for j := 0; j < len(array2D[i]); j++ {
				arrayTemp = append(arrayTemp, array2D[i][j])
			}
			arrayOld2D = append(arrayOld2D, arrayTemp)
			arrayTemp = []string{}
		}
		//Iterate through each char
		for i := 0; i < len(arrayOld2D); i++ {
			for l := 0; l < len(arrayOld2D[i]); l++ {
				nearSeats = 0
				// Checks for floor spaces and disqualifies them
				if string(arrayOld2D[i][l]) == "." {
					nearSeats -= 120
				}
				// Checks left
				for m := l - 1; m >= 0; m-- {
					if string(arrayOld2D[i][m]) == "#" {
						nearSeats++
						m -= 120
					} else if string(arrayOld2D[i][m]) == "L" {
						m -= 120
					}
				}
				// Checks right
				for m := l + 1; m < len(arrayOld2D[i]); m++ {
					if string(arrayOld2D[i][m]) == "#" {
						nearSeats++
						m += 120
					} else if string(arrayOld2D[i][m]) == "L" {
						m += 120
					}
				}
				// Checks up
				for n := i - 1; n >= 0; n-- {
					if string(arrayOld2D[n][l]) == "#" {
						nearSeats++
						n -= 120
					} else if string(arrayOld2D[n][l]) == "L" {
						n -= 120
					}
				}
				// Checks down
				for n := i + 1; n < len(arrayOld2D); n++ {
					if string(arrayOld2D[n][l]) == "#" {
						nearSeats++
						n += 120
					} else if string(arrayOld2D[n][l]) == "L" {
						n += 120
					}
				}
				// Checks <^
				n := l - 1
				for m := i - 1; m >= 0; m-- {
					if n >= 0 {
						if string(arrayOld2D[m][n]) == "#" {
							nearSeats++
							m -= 120
						} else if string(arrayOld2D[m][n]) == "L" {
							m -= 120
						}
					} else {
						m -= 120
					}
					n--
				}
				// Checks >^
				n = l + 1
				for m := i - 1; m >= 0; m-- {
					if n < len(arrayOld2D[m]) {
						if string(arrayOld2D[m][n]) == "#" {
							nearSeats++
							m -= 120
						} else if string(arrayOld2D[m][n]) == "L" {
							m -= 120
						}
					} else {
						m -= 120
					}
					n++
				}
				// Checks <\/
				n = l - 1
				for m := i + 1; m < len(arrayOld2D); m++ {
					if n >= 0 {
						if string(arrayOld2D[m][n]) == "#" {
							nearSeats++
							m += 120
						} else if string(arrayOld2D[m][n]) == "L" {
							m += 120
						}
					} else {
						m += 120
					}
					n--
				}
				// Checks >\/
				n = l + 1
				for m := i + 1; m < len(arrayOld2D); m++ {
					if n < len(arrayOld2D[m]) {
						if string(arrayOld2D[m][n]) == "#" {
							nearSeats++
							m += 120
						} else if string(arrayOld2D[m][n]) == "L" {
							m += 120
						}
					} else {
						m += 120
					}
					n++
				}
				// print(nearSeats, " ")
				if nearSeats == 0 { // now either empty or fill
					array2D[i][l] = "#"
				} else if nearSeats >= 5 {
					array2D[i][l] = "L"
				}
			}
			// println()
		}
		// for i := 0; i < len(arrayOld2D); i++ {
		// 	fmt.Println(arrayOld2D[i])
		// }
		// println("----")
		// for i := 0; i < len(array2D); i++ {
		// 	fmt.Println(array2D[i])
		// }
	}
	// Now find the amount occupied
	for i := 0; i < len(array2D); i++ {
		for l := 0; l < len(array2D[i]); l++ {
			if string(array2D[i][l]) == "#" {
				ocupado++
			}
		}
	}
	return ocupado
}

// Make 2D
func make2D(array []string) [][]string {
	// Now make it 2d
	array2D := [][]string{}
	tempArray := []string{}
	for i := 0; i < len(array); i++ {
		for l := 0; l < len(array[i]); l++ {
			tempArray = append(tempArray, string(array[i][l]))
		}
		array2D = append(array2D, tempArray)
		tempArray = []string{}
	}
	return array2D
}

// Import text
func importText(fileName string) []string {
	// open text file
	f, err := os.Open(fileName)
	//checks for error
	if err != nil {
		log.Fatal(err)
	}
	//tells it to close the file once program dies
	defer f.Close()
	// Defines list to be appended to
	listOfThings := []string{}
	// Imports file scanner
	scanner := bufio.NewScanner(f)
	//Goes line by line and scans
	for scanner.Scan() {
		temp := scanner.Text()
		listOfThings = append(listOfThings, temp)
	}
	// checks for errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return listOfThings
}
