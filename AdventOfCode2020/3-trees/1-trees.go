package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	// open text file
	f, err := os.Open("input.txt")
	//checks for error
	if err != nil {
		log.Fatal(err)
	}
	//tells it to close the file once program dies
	defer f.Close()
	// Defines list to be appended to
	listOfWords := []string{}
	// Imports file scanner
	scanner := bufio.NewScanner(f)
	//Goes line by line and scans
	for scanner.Scan() {
		temp := scanner.Text()
		if err != nil {
			log.Fatal(err)
		}
		listOfWords = append(listOfWords, temp)
	}
	// checks for errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	// Creates second list
	extendedListOfWords := []string{}
	for i, var1 := range listOfWords {
		widthMult := len(listOfWords) / 10
		var1Extend := strings.Repeat(var1, widthMult)
		extendedListOfWords = append(extendedListOfWords, var1Extend)
		_, _ = i, var1
	}
	// Now do the iteration and check for trees
	totalTrees := 0
	counter := 0
	for c, var2 := range extendedListOfWords {
		locTemp := string(var2[counter])
		fmt.Println(locTemp)
		fmt.Println(var2)
		counter += 3
		if locTemp == "#" {
			totalTrees++
		}
		_, _ = c, var2
	}
	fmt.Println(totalTrees)
}
