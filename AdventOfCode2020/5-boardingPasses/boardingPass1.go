package main

import (
	"bufio"
	"log"
	"os"
)

func main() {
	listOfSeats := importText("input.txt")
	maxID := 0
	for i := 0; i < len(listOfSeats); i++ {
		row := findRow(listOfSeats[i])
		col := findColumn(listOfSeats[i])
		id := row*8 + col
		if id > maxID {
			maxID = id
		}
	}
	println("MaxID:", maxID)
	array2D := emtpyArray()
	for i := 0; i < len(listOfSeats); i++ {
		row := findRow(listOfSeats[i])
		col := findColumn(listOfSeats[i])
		array2D[row][col] = true
	}
	// Define my seat row and col
	rowFinal := 0
	colFinal := 0
	for i := 0; i < len(array2D); i++ {
		// print(i)
		for c := 0; c < len(array2D[i]); c++ {
			// print(array2D[i][c])
			if i < 100 && i > 20 && array2D[i][c] == false {
				rowFinal = i
				colFinal = c
			}
		}
		println()
	}
	println("My seat ID:", rowFinal*8+colFinal)
}

// Finds the row
func findRow(stringToParse string) int {
	listOfNums := []int{}
	for i := 0; i < 128; i++ {
		listOfNums = append(listOfNums, i)
	}
	for i := 0; i < 7; i++ {
		if string(stringToParse[i]) == "F" {
			listOfNums = listOfNums[:len(listOfNums)/2]
		} else if string(stringToParse[i]) == "B" {
			listOfNums = listOfNums[len(listOfNums)/2:]
		}
	}
	return listOfNums[0]
}

// Finds the column
func findColumn(stringToParse string) int {
	listOfNums := []int{}
	for i := 0; i < 8; i++ {
		listOfNums = append(listOfNums, i)
	}
	for i := 7; i < len(stringToParse); i++ {
		if string(stringToParse[i]) == "L" {
			listOfNums = listOfNums[:len(listOfNums)/2]
		} else if string(stringToParse[i]) == "R" {
			listOfNums = listOfNums[len(listOfNums)/2:]
		}
	}
	return listOfNums[0]
}

// Import text
func importText(fileName string) []string {
	// open text file
	f, err := os.Open(fileName)
	//checks for error
	if err != nil {
		log.Fatal(err)
	}
	//tells it to close the file once program dies
	defer f.Close()
	// Defines list to be appended to
	listOfThings := []string{}
	// Imports file scanner
	scanner := bufio.NewScanner(f)
	//Goes line by line and scans
	for scanner.Scan() {
		temp := scanner.Text()
		listOfThings = append(listOfThings, temp)
	}
	// checks for errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return listOfThings
}

// Make empty bool array
func emtpyArray() [][]bool {
	empty2D := [][]bool{}
	tempEmpty := []bool{}
	for i := 0; i < 128; i++ {
		for c := 0; c < 8; c++ {
			tempEmpty = append(tempEmpty, false)
		}
		empty2D = append(empty2D, tempEmpty)
		tempEmpty = []bool{}
	}
	return empty2D
}
