package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	unParsedList := importText("input.txt")
	intList := parseList(unParsedList)
	number := findNumber(intList, 30000000)
	fmt.Println(number)
}

// Finds number at specific turn
func findNumber(turnList []int, location int) int {
	thingMap := make(map[int]int)
	// Adds all of the values to the map besides the 3rd one
	for i := 0; i < len(turnList)-1; i++ {
		thingMap[turnList[i]] = i + 1
		println("turn", i+1, "is", turnList[i])
	}
	prev := turnList[len(turnList)-1]
	println("turn 3 is", prev)
	for i := len(turnList) + 1; i <= location; i++ {
		// println("---------")
		// fmt.Println(thingMap)
		// Now checks if the previous is in the map
		if val, ok := thingMap[prev]; ok {
			// If it is do the math to find the value for this iteration
			// println("turn", i-1)
			// println("turn", thingMap[prev])
			val = i - 1 - thingMap[prev]
			//  add this iteration to the map
			thingMap[prev] = i - 1
			//  set prev to the current iteration
			prev = val
			// println("turn", i, "is", val)
			_, _ = val, ok
		} else {
			// If this value doesn't exist yet, set 0 to this index
			thingMap[prev] = i - 1
			prev = 0
			// println("turn", i, "is", 0)
		}
		// println("turn", i, "is", prev)
	}
	return prev
}

// parses list
func parseList(list []string) []int {
	splitted := strings.Split(list[0], ",")
	intList := []int{}
	for i := 0; i < len(splitted); i++ {
		temp, _ := strconv.Atoi(splitted[i])
		intList = append(intList, temp)
	}
	return intList
}

// Import text
func importText(fileName string) []string {
	// open text file
	f, err := os.Open(fileName)
	//checks for error
	if err != nil {
		log.Fatal(err)
	}
	//tells it to close the file once program dies
	defer f.Close()
	// Defines list to be appended to
	listOfThings := []string{}
	// Imports file scanner
	scanner := bufio.NewScanner(f)
	//Goes line by line and scans
	for scanner.Scan() {
		temp := scanner.Text()
		listOfThings = append(listOfThings, temp)
	}
	// checks for errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return listOfThings
}
