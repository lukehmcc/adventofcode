package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	// PART ONE
	// input text
	rules := importText("input.txt")
	// Grab valid list of nums
	validNums := pullValidNums(rules)
	// fmt.Println("valid numbers:", validNums)
	// Grab list of NEARBY tickets
	nearbyTicks := grabNearBy(rules)
	// fmt.Println("nearby tickets:", nearbyTicks)
	// Get back a list of valid index + list of invalid indexes
	validityIndex := ticketChecker(validNums, nearbyTicks)
	// fmt.Println("validity index:", validityIndex)
	// Calculate error
	errorCheck := findError(nearbyTicks, validityIndex)
	fmt.Println("error:", errorCheck)

	// PART TWO
	// Discard bad tickets
	nearByTicksRevised := eliminateBadTicks(nearbyTicks, validityIndex)
	// make map of correct nums
	ruleMap := makeMap(rules)
	// Go through and find ordered list of params
	orderedKey := orderFinder(ruleMap, nearByTicksRevised)
	fmt.Println(orderedKey)
}

// Makes a list of valid numbers
func pullValidNums(rules []string) []int {
	// Pulls out the rule lines
	lineParse := []string{}
	for i := 0; i < len(rules); i++ {
		if strings.Index(rules[i], "-") > -1 {
			lineParse = append(lineParse, rules[i])
		}
	}
	// Pulls out the ranges
	validNums := []int{}
	listOfRanges := []string{}
	for i := 0; i < len(lineParse); i++ {
		tempList := []string{}
		tempList = strings.Split(lineParse[i], " ")
		for j := 0; j < len(tempList); j++ {
			if _, err := strconv.Atoi(string(tempList[j][0])); err == nil {
				listOfRanges = append(listOfRanges, tempList[j])
			}
		}
	}
	// Makes the valid num list
	for i := 0; i < len(listOfRanges); i++ {
		tempList := strings.Split(listOfRanges[i], "-")
		lowBound, _ := strconv.Atoi(tempList[0])
		highBound, _ := strconv.Atoi(tempList[1])
		for j := lowBound; j <= highBound; j++ {
			validNums = append(validNums, j)
		}
	}
	return validNums
}

// Grabs nearby
func grabNearBy(rules []string) [][]int {
	nearbyTicks := [][]int{}
	nearbyIndex := 0
	// Finds beginnign index of nearby tickets
	for i := 0; i < len(rules); i++ {
		if strings.Contains(rules[i], "nearby") {
			nearbyIndex = i + 1
		}
	}
	rulesNearby := rules[nearbyIndex:]
	for i := 0; i < len(rulesNearby); i++ {
		tempArray := []int{}
		tempArray2 := strings.Split(rulesNearby[i], ",")
		for j := 0; j < len(tempArray2); j++ {
			temp, _ := strconv.Atoi(tempArray2[j])
			tempArray = append(tempArray, temp)
		}
		nearbyTicks = append(nearbyTicks, tempArray)
	}
	return nearbyTicks
}

// checks if ticket is valid or not and returns list of valid & invalid indexes
func ticketChecker(validNums []int, nearbyTicket [][]int) [][]bool {
	// Initialized 2d array
	validList := [][]bool{}
	for i := 0; i < len(nearbyTicket); i++ {
		validTemp := []bool{}
		for j := 0; j < len(nearbyTicket[i]); j++ {
			validTemp = append(validTemp, false)
		}
		validList = append(validList, validTemp)
	}
	// Checks which values are possible
	for i := 0; i < len(nearbyTicket); i++ {
		for j := 0; j < len(nearbyTicket[i]); j++ {
			for k := 0; k < len(validNums); k++ {
				if nearbyTicket[i][j] == validNums[k] {
					validList[i][j] = true
				}
			}
		}
	}
	return validList
}

// Calculates error
func findError(nearbyTicks [][]int, validityIndex [][]bool) int {
	errorNum := 0
	for i := 0; i < len(validityIndex); i++ {
		for j := 0; j < len(validityIndex[i]); j++ {
			if validityIndex[i][j] == false {
				errorNum += nearbyTicks[i][j]
			}
		}
	}
	return errorNum
}

// Eliminates bad tickets
func eliminateBadTicks(nearbyTicks [][]int, validityIndex [][]bool) [][]int {
	nearbyTicksValid := [][]int{}
	for i := 0; i < len(nearbyTicks); i++ {
		checker := 0
		for j := 0; j < len(nearbyTicks[i]); j++ {
			if validityIndex[i][j] {
				checker++
			}
		}
		if checker == len(nearbyTicks[i]) {
			nearbyTicksValid = append(nearbyTicksValid, nearbyTicks[i])
		}
	}
	return nearbyTicksValid
}

// Makes a map from rules
func makeMap(rules []string) map[string][]int {
	// Creates a map
	ruleMap := make(map[string][]int)
	// Iterates through the rules
	for i := 0; i < len(rules); i++ {
		// Only operates with rules that contain ranges
		if strings.Contains(rules[i], "-") {
			// println(rules[i])
			// Grabs the title of the range
			title := rules[i][:strings.Index(rules[i], ":")]
			// Defines the array key for the map
			ruleMap[title] = []int{}
			// Splits up the rule string by spaces
			rulesSplit := strings.Split(rules[i], " ")
			// iterates through that split up string
			for j := 0; j < len(rulesSplit); j++ {
				if strings.Contains(rulesSplit[j], "-") {
					// println(rulesSplit[j])
					tempList := strings.Split(rulesSplit[j], "-")
					// fmt.Println(tempList)
					// Checks to make sure the first value is a number
					if _, err := strconv.Atoi(string(tempList[0])); err == nil {
						// It then iterates through this range and adds all possible
						// ints to the array
						lowBound, _ := strconv.Atoi(tempList[0])
						highBound, _ := strconv.Atoi(tempList[1])
						for j := lowBound; j <= highBound; j++ {
							ruleMap[title] = append(ruleMap[title], j)
						}
					}
				}
			}
		}
	}
	return ruleMap
}

//I know global vars are bad but it's too easy here
var justKeys = [][]string{}

// finds the order
func orderFinder(ruleMap map[string][]int, nearByTicksRevised [][]int) []string {
	orderedKeys := []string{}
	tempList := []string{}
	for key, value := range ruleMap {
		tempList = append(tempList, key)
		_ = value
	}
	// Create those permutations
	heapPermutation(tempList, len(tempList))
	// Now test all the things
	for i := 0; i < len(justKeys); i++ {
		println("Possible:", len(justKeys))
		println("Current:", i)
	out:
		for j := 0; j < len(nearByTicksRevised); j++ {
			for k := 0; k < len(nearByTicksRevised[j]); k++ {
				if isInList(ruleMap[justKeys[i][k]], nearByTicksRevised[j][k]) == false {
					break out
				}
			}
		}
		orderedKeys = justKeys[i]
	}
	return orderedKeys
}

func isInList(list []int, val int) bool {
	for i := 0; i < len(list); i++ {
		if list[i] == val {
			return true
		}
	}
	return false
}

// heap permutation that I definately wrote myself
func heapPermutation(a []string, size int) {
	if size == 1 {
		justKeys = append(justKeys, a)
		println(len(justKeys))
	}
	for i := 0; i < size; i++ {
		heapPermutation(a, size-1)

		if size%2 == 1 {
			a[0], a[size-1] = a[size-1], a[0]
		} else {
			a[i], a[size-1] = a[size-1], a[i]
		}
	}
}

// Import text
func importText(fileName string) []string {
	// open text file
	f, err := os.Open(fileName)
	//checks for error
	if err != nil {
		log.Fatal(err)
	}
	//tells it to close the file once program dies
	defer f.Close()
	// Defines list to be appended to
	listOfThings := []string{}
	// Imports file scanner
	scanner := bufio.NewScanner(f)
	//Goes line by line and scans
	for scanner.Scan() {
		temp := scanner.Text()
		listOfThings = append(listOfThings, temp)
	}
	// checks for errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return listOfThings
}
