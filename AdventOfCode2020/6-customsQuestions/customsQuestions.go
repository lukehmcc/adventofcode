package main

import (
	"bufio"
	"log"
	"os"
	"strings"
)

func main() {
	listOfAnswers := importText("input.txt")
	listOfAnswersCompact := parseGroups(listOfAnswers)
	listOfLetters := "abcdefghijklmnopqrstuvwxyz"
	sumYes := 0
	for i := 0; i < len(listOfAnswersCompact); i++ {
		for c := 0; c < len(listOfLetters); c++ {
			if strings.Contains(listOfAnswersCompact[i], string(listOfLetters[c])) {
				sumYes++
			}
		}
	}
	println("Sum of yeses:", sumYes)
	sumYesAgree := 0
	temp := true
	listOfAnswers2D := parseGroups2D(listOfAnswers)
	for i := 0; i < len(listOfAnswers2D); i++ {
		for c := 0; c < len(listOfLetters); c++ {
			temp = true
			for g := 0; g < len(listOfAnswers2D[i]); g++ {
				if strings.Contains(listOfAnswers2D[i][g], string(listOfLetters[c])) != true {
					temp = false
				}
			}
			if temp == true {
				sumYesAgree++
			}
		}
	}
	println("Sum of those who agree:", sumYesAgree)
}

// Import text
func importText(fileName string) []string {
	// open text file
	f, err := os.Open(fileName)
	//checks for error
	if err != nil {
		log.Fatal(err)
	}
	//tells it to close the file once program dies
	defer f.Close()
	// Defines list to be appended to
	listOfThings := []string{}
	// Imports file scanner
	scanner := bufio.NewScanner(f)
	//Goes line by line and scans
	for scanner.Scan() {
		temp := scanner.Text()
		listOfThings = append(listOfThings, temp)
	}
	// checks for errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return listOfThings
}

// Parses for groups
func parseGroups(listOfThings []string) []string {
	listOfThingsCompact := []string{}
	temp := ""
	for i := 0; i < len(listOfThings); i++ {
		if listOfThings[i] == "" || len(listOfThings)-1 == i {
			listOfThingsCompact = append(listOfThingsCompact, temp)
			temp = ""
		} else {
			temp += listOfThings[i]
		}
	}
	listOfThingsCompact[len(listOfThingsCompact)-1] += listOfThings[len(listOfThings)-1]
	return listOfThingsCompact
}

// Parses for groups
func parseGroups2D(listOfThings []string) [][]string {
	listOfThings2D := [][]string{}
	temp := []string{}
	for i := 0; i < len(listOfThings); i++ {
		if listOfThings[i] == "" || len(listOfThings)-1 == i {
			listOfThings2D = append(listOfThings2D, temp)
			temp = []string{}
		} else {
			temp = append(temp, listOfThings[i])
		}
	}
	listOfThings2D[len(listOfThings2D)-1] = append(listOfThings2D[len(listOfThings2D)-1], listOfThings[len(listOfThings)-1])
	return listOfThings2D
}
